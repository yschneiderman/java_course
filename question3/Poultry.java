
public class Poultry extends Animal{

	public Poultry(String name, int age, String color) {
		super(name, age, color);
		// TODO Auto-generated constructor stub
	}
	
	public void fly() {
		System.out.println("Poultry is flying");
	}
	
	@Override
	public String toString() {
		return "Name: " + this.getName() + " Color: " + this.getColor() + " Age: " + this.getAge(); 
	}

}
