import java.util.Objects;

public class Dog extends Mammal{
	private Owner owner;
	
	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public Dog(String name, int age, String color, Owner owner) {
		super(name, age, color);
		this.owner = owner;
	}
	
	@Override
	public void eat() {
		System.out.println(this.getName() +" is eating");
	}
	
	@Override
	public void sleep() {
		System.out.println(this.getName() + " slepping");
	} 
	
	@Override
	public void run() {
		System.out.println(this.getName() + " is running");
	}
	
	public void bark() {
		System.out.println(this.getName() + " is barking");
	} 
	
	@Override
	public String toString() {
		return super.toString();
	}
	
	public boolean equals(Dog dog) {
	    return Objects.equals(this.getName(), dog.getName())
	            && Objects.equals(this.getAge(), dog.getAge())
	            && Objects.equals(this.getColor(), dog.getColor())
	            && this.owner.equals(dog.getOwner());
	}
}
