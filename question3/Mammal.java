
public abstract class Mammal extends Animal{

	public Mammal(String name, int age, String color) {
		super(name, age, color);
	}
	
	abstract public void run();
}
