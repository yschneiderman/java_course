
public class Bird extends Poultry{

	public Bird(String name, int age, String color) {
		super(name, age, color);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void eat() {
		System.out.println(this.getName() +" is eating");
	}
	
	@Override
	public void sleep() {
		System.out.println(this.getName() + " slepping");
	} 
	
	@Override
	public void fly() {
		System.out.println(this.getName() + " is flying");
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
}
