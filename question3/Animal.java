import java.util.Objects;

public class Animal {
	private String name;
	private int age;
	private String color;	
	
	public Animal(String name, int age, String color) {
		this.name = name;
		this.age = age;
		this.color = color;
	}
	
	
	public void eat() {
		System.out.println("animal eating");
	}
	
	public void sleep() {
		System.out.println("animal slepping");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	@Override
	public String toString() {
		return "Name: " + this.getName() + " Color: " + this.getColor() + " Age: " + this.getAge(); 
	}
	
	public boolean equals(Dog dog) {
	    return Objects.equals(this.getName(), dog.getName())
	            && Objects.equals(this.getAge(), dog.getAge())
	            && Objects.equals(this.getColor(), dog.getColor());
	}
}
