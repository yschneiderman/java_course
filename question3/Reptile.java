
public class Reptile extends Animal{

	public Reptile(String name, int age, String color) {
		super(name, age, color);
		// TODO Auto-generated constructor stub
	}
	
	public void crawl() {
		System.out.println("Reptile is crawling");
	}
	
	@Override
	public String toString() {
		return "Name: " + this.getName() + " Color: " + this.getColor() + " Age: " + this.getAge(); 
	}

}
