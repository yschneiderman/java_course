package question4;

import java.awt.Color;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

/*************************************************************************
 * 							Cell class
 * Represent label cell with active boolean value and position
 ************************************************************************/ 
public class Cell {
	JLabel label;
	int column;
	int row;
	boolean isActive; 

	Random random = new Random();
	public Cell(int column, int row) {
		this.column = column;
		this.row = row;
		this.isActive = random.nextBoolean();
		this.label = new JLabel("");
		this.label.setOpaque(true);
		this.label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.label.setBackground(this.getColor());
	}
	
	
	/******************************************************************
	 *Return the current cell color -> if active return gray else white
	 ******************************************************************/ 
	public Color getColor() {
		return isActive ? Color.GRAY : Color.WHITE;
	}
}
