package question4;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MatrixLife {
	private int col;
	private int row;
	private Cell matrix[][];
	private boolean tempMatrix[][];	
	private JFrame frame;
	private JPanel panel;
	
	public MatrixLife(int col, int row) {
		this.col = col;
		this.row = row;
		this.matrix = new Cell [col][row];
		this.tempMatrix = new boolean [col][row];
		
		initMatrix();
		this.yesNoNextGen();
	}
	
	private void initMatrixWithRandomValues() {
		for(int i = 0 ; i < matrix.length ; i++) {
			for(int j = 0 ; j < matrix[i].length; j++) {
				matrix[i][j] = new Cell(i,j);
			}
		}
	}
	
	/********************
	 * Draw panel labels
	*********************/
	public void drawLabels() {
		for(int i = 0 ; i < matrix.length ; i++) {
			for(int j = 0 ; j < matrix[i].length; j++) {
				panel.add(matrix[i][j].label);
			}
		}
	}
	
	public void setGuiOptions() {
		frame = new JFrame("Mamma 12 Question 2");
		frame.setVisible(true);
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel(new GridLayout(this.col, this.row));
		panel.setBackground(Color.BLACK);
		frame.add(panel);
	}
	
	/***************************************
	 * Initialize matrix with random values 
	****************************************/
	public void initMatrix() {
		this.initMatrixWithRandomValues();
		this.setGuiOptions();
		this.drawLabels();	
	}	
	
	/*****************
	 * Refresh matrix 
	*******************/
	private void refreshMatrix() {
		for(int i = 0 ; i < matrix.length ; i++) {
			for(int j = 0 ; j < matrix[i].length; j++) {
				this.matrix[i][j].isActive = this.tempMatrix[i][j];
				this.matrix[i][j].label.setBackground(this.matrix[i][j].getColor());	
			}
		}
	}
	
	/****************************
	 * Calculate life neighbors
	*****************************/
	private int calculateLifeNeighbors(int  col, int row) {
		int active = 0;
	     //find all serouding cell by adding +/- 1 to col and row 
	    for (int colNum = col - 1 ; colNum <= (col + 1) ; colNum +=1  ) {
	        for (int rowNum = row - 1 ; rowNum <= (row + 1) ; rowNum +=1  ) {
	             //if not the center cell 
	            if(! ((colNum == col) && (rowNum == row))) {
	                //make sure it is within  grid
	                if(withinGrid (colNum, rowNum) && this.matrix[colNum][rowNum].isActive) {
	                    active++;
	                }
	            }
	        }
	    }
	    return active;
	}
	
	
	/*************************************************************
	 * Return true if the cell inside the grid else return false
	**************************************************************/
	private boolean withinGrid(int colNum, int rowNum) {
	    if((colNum < 0) || (rowNum < 0) ) {
	        return false;
	    }
	    if((colNum >= this.col) || (rowNum >= this.row)) {
	        return false;
	    }
	    return true;
	}
	
	/******************************************************************************
	 * The cell has no life and three of its neighbors have life -> Will be born
	*******************************************************************************/	
	private boolean isBirthState(int i, int j, int activeNeighbors, boolean isActive) {
		if(!isActive && activeNeighbors == 3) {
			this.tempMatrix[i][j] = true;
			return true;
		}else {
			return false;
		}
	}
	
	/******************************************************************************
	 * The cell has life and one or none of its neighbors have life -> will die
	 * The cell has life and four or more of its neighbors have life - > will die
	*******************************************************************************/	
	private boolean isDead(int i, int j, int activeNeighbors, boolean isActive) {
		if(isActive && (activeNeighbors <= 1 || activeNeighbors >=4)) {
			this.tempMatrix[i][j] = false;
			return true;
		}
		return false;
	}
	

	/******************************************************************************************
	 * The cell has life and two or three of its neighbors have life - > Will continue to live
	*******************************************************************************************/	
	private boolean isExistence(int i , int j, int activeNeighbors, boolean isActive){
		if(isActive && (activeNeighbors == 2 || activeNeighbors == 3)) {
			this.tempMatrix[i][j] = true;
		}
		return false;
	}
	
	/********************************
	 * Calculate the next generation
	*********************************/	
	private void calculateNextGeneration() {
		for(int i = 0 ; i < matrix.length ; i++) {
			for(int j = 0 ; j < matrix[i].length; j++) {
				int activeNeighbors = this.calculateLifeNeighbors(i, j);
				boolean isActive = this.matrix[i][j].isActive;
				
				this.isBirthState(i,j, activeNeighbors, isActive);
				this.isDead(i,j, activeNeighbors, isActive);
				this.isExistence(i,j, activeNeighbors, isActive);	
			}
		}
		this.refreshMatrix();
		this.yesNoNextGen();
	}
	
	/********************************
	 * Present Yes/No/Cancel dialog
	 * yes : Call calculateNextGeneration to calculate the next generation
	 * no : Do nothing
	*********************************/
	private void yesNoNextGen() {
		int reply = JOptionPane.showConfirmDialog(null, "Do you want see next generation", null, JOptionPane.YES_NO_CANCEL_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
        	this.calculateNextGeneration();
        }
	}
}
