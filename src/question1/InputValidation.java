package question1;

import java.util.HashMap;

/******************************
 * User input validation class
*******************************/
public class InputValidation {
	/*************************************************************************
	 * return true if user input not contains duplicate 
	 * digits and the input length is correct and not contains not digits
	*************************************************************************/
	boolean validation(String attempt, int maxLength) {
		if (lengthValidation(attempt, maxLength) && onlyDigitValidation(attempt)
				&& onlyUniqueDigitsValidation(attempt)) {
			return true;
		}
		return false;
	}
	
	/*********************************************
	 * return true if user input length is correct
	**********************************************/
	boolean lengthValidation(String attempt, int maxLength) {
		if (attempt.length() != maxLength) {
			System.out.println("attempt should contains " + maxLength + " digits");
			return false;
		}
		return true;
	}

	/***************************************************
	 * return true if user input contains only digits
	****************************************************/
	boolean onlyDigitValidation(String attempt) {
		if (!attempt.matches("\\d+")) {
			System.out.println("attempt should contains only digits");
			return false;
		}
		return true;
	}

	/********************************************************
	 * return true if user input contains only unique digits
	*********************************************************/
	boolean onlyUniqueDigitsValidation(String attempt) {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < attempt.length(); i++) {
			int num = Character.getNumericValue(attempt.charAt(i));
			if (map.containsKey(num)) {
				System.out.println("attempt should not contains duplicate digits");
				return false;
			}
			map.put(num, i);
		}
		return true;
	}
}
