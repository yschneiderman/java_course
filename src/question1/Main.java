package question1;

public class Main {

	public static void main(String[] args) {
		
		NumberSelector numberSelector = new NumberSelector(4);
		
		String attempt = numberSelector.getUserAttemp();
		while(!numberSelector.calculateScore(attempt)) {
			attempt = numberSelector.getUserAttemp();
		}
		System.out.println("You've been able to find the right number");
		
	}

}
