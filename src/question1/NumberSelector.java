package question1;

import java.util.Random;
import java.util.Scanner;
import java.util.HashMap;

public class NumberSelector {
	static final int MIN_RANDOM_NUMBER = 0;
	static final int MAX_RANDOM_NUMBER = 9;
	
	private Scanner reader;
	private int numberOfDigits = 4;
	HashMap<Integer, Integer> randomNumberHashMap;
	InputValidation inputValidation = new InputValidation();	
	
	/*******************************************************
	 * Constructor - generate random K digits
	 * Using HashMap<Integer, Integer> : HashMap<KEY, INDEX> 
	********************************************************/
	public NumberSelector(int numberOfDigits){
		this.numberOfDigits = numberOfDigits;
		Random rng = new Random();
		int index = 0;
		this.randomNumberHashMap = new HashMap<>();

		while (this.randomNumberHashMap.size() < this.numberOfDigits) {
			Integer next = rng.nextInt(MAX_RANDOM_NUMBER) + 1;
			if (!this.randomNumberHashMap.containsKey(next)) {
				this.randomNumberHashMap.put(next, index++);
			}
		}
		System.out.println(this.randomNumberHashMap);
	}
	
	/*************************************
	 * Get user attempt - K digit (String)
	***************************************/
	String getUserAttemp() {
		reader = new Scanner(System.in);
		System.out.println("Enter " + this.numberOfDigits + " digits number: ");
		return reader.next();
	}


	/***************************
	 * Calculate user score
	****************************/
	boolean calculateScore(String attempt) {
		if (inputValidation.validation(attempt, this.numberOfDigits)) {
			int countExactly = 0;
			int countAlmost = 0;
			for (int i = 0; i < attempt.length(); i++) {
				int digit = Character.getNumericValue(attempt.charAt(i));
				if (this.randomNumberHashMap.containsKey(digit)
						&& this.randomNumberHashMap.get(digit).intValue() == i) {
					countExactly++;
				} else if (this.randomNumberHashMap.containsKey(digit)) {
					countAlmost++;
				}
			}
			if(countExactly == this.numberOfDigits) {
				return true;
			}else {
				System.out.println("Exactly: " + countExactly + " , Almost: " + countAlmost);
				return false;
			}
			
		} else {
			
			return false;
		}

	}
}
