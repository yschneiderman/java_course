package question2;

public class Card {
	private Faces face;
	private  String suit;
	
	public void setSuit(String suit) {
		this.suit = suit;
	}

	public void setFace(Faces face) {
		this.face = face;
	}

	public Faces getFace() {
		return face;
	}

	public String getSuit() {
		return suit;
	}
	
	public Card(Faces cardFace, String cardSuit) {
		this.face = cardFace;
		this.suit = cardSuit;
	}
	
	public int compare(Card b) {
		int faceAVal = this.getFace().getValue();
		int faceBVal = b.getFace().getValue();
		if(faceAVal > faceBVal) {
			return 1;
		}else if(faceAVal < faceBVal) {
			return -1;
		}else {
			return 0;
		}
	}
	
	
	public String toString() {
		return face + " of " + suit;
	}
}
