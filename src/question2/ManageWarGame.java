package question2;

import java.util.ArrayList;
import java.util.Arrays;

public class ManageWarGame {
	Player playerA;
	Player playerB;

	public ManageWarGame() {
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
		startGame();
	}
	
	private void startGame() {
		this.playerA = new Player();
		this.playerB = new Player();
		
		do {
			ArrayList<Card> playerACards = new ArrayList<Card>(Arrays.asList(this.playerA.deck.dealCard()));
			ArrayList<Card> playerBCards = new ArrayList<Card>(Arrays.asList(this.playerB.deck.dealCard()));

			play(this.playerA, this.playerB, playerACards, playerBCards);
		} while (getWinner(this.playerA, this.playerB));
	}

	public void play(Player playerA, Player playerB, ArrayList<Card> playerACards, ArrayList<Card> playerBCards) {
		
		if (playerACards != null && playerBCards != null) {
			Card cardALast = playerACards.get(playerACards.size() - 1);
			Card cardBLast = playerBCards.get(playerBCards.size() - 1);
			if ((cardALast != null && cardALast.getFace() != null)
					&& (cardBLast != null && cardBLast.getFace() != null)) {
				int compare = cardALast.compare(cardBLast);

				switch (compare) {
				case 1: // a > b
					playerACards.addAll(playerBCards);
					playerA.deck.addAll(playerACards);

//					System.out.println(
//							"Player A with: " + (cardALast.toString()) + " > Player B with: " + (cardBLast.toString()));
//					System.out.println();
					break;
				case -1: // a < b
					playerBCards.addAll(playerACards);
					playerB.deck.addAll(playerBCards);

//					System.out.println(
//							"Player B with: " + (cardBLast.toString()) + " > Player A with: " + (cardALast.toString()));
//					System.out.println();
					break;
				case 0: // a == b
//					System.out.println("WAR : Player A with: " + (cardALast.toString()) + " equals Player B with: "
//							+ (cardBLast.toString()));
//					System.out.println();
					
					playerACards.addAll(playerA.deck.deal3Cards());
					playerBCards.addAll(playerB.deck.deal3Cards());
					play(playerA, playerB, playerACards, playerBCards);
				default:
					return;
				}
			}

		}
	}

	private boolean getWinner(Player playerA, Player playerB) {
		if (playerA.deck.size() == 0) {
			System.out.println("Player B winning the game");
			return false;
		} else if (playerB.deck.size() == 0) {
			System.out.println("Player A winning the game");
			return false;
		}
		return true;
	}

}
