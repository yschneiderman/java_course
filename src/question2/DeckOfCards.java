package question2;

import java.security.SecureRandom;
import java.util.ArrayList;

public class DeckOfCards {
	private ArrayList<Card> deck;
	private static final int NUMBER_OF_CARDS = 52;
	private static final SecureRandom randomNumbers = new SecureRandom();
	
	public  DeckOfCards() {
		String[] faces = {Faces.Ace.name(), Faces.Deuce.name(), Faces.Three.name(), Faces.Four.name(), Faces.Five.name(), Faces.Six.name(), Faces.Seven.name(), Faces.Eight.name(), Faces.Nine.name() , Faces.Ten.name(), Faces.Jack.name(), Faces.Queen.name(), Faces.King.name()};
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		
		deck = new ArrayList<Card>();
		int count = 0;
	
		while(count < NUMBER_OF_CARDS) {
			deck.add(new Card(Faces.valueOf(faces[count%13]), suits[count/13]));
			count++;
		}
	}
	
	//shuffle deck of Cards with one-pass algorithm
	public void shuffle() {
		for(int first = 0; first< deck.size(); first++) {
			int second = randomNumbers.nextInt(NUMBER_OF_CARDS);
			swap(first,second);
		}
	}
	
	//get deck size
	public int size() {
		return this.deck.size();
	}
	
	public void swap(int indexA, int indexB) {
		Card temp = deck.get(indexA);
		deck.get(indexA).setFace(deck.get(indexB).getFace());
		deck.get(indexA).setSuit(deck.get(indexB).getSuit());
			
		deck.get(indexB).setFace(temp.getFace());
		deck.get(indexB).setSuit(temp.getSuit());
	}
	
	//deal one Card
	public Card dealCard() {
		if(deck.size() > 0) {
			return deck.remove(0);
		}else {
			return null;
		}
	}
	
	public ArrayList<Card> deal3Cards() {
		ArrayList<Card> result = new ArrayList<Card>();
		if(deck.size() > 0) {
			
			int count = 0;
			while(count != 2 && deck.size() > 1) {
				result.add(deck.remove(0));
				count++;
			}
			result.add(deck.remove(0));
			return result;
		}
		return result;
	}
	
	public void add(Card card) {
		this.deck.add(card);
	}
	
	public void addAll(ArrayList<Card> cards) {
		this.deck.addAll(cards);
	}
	
	public void printDeck() {
		for(int count = 0 ; count < deck.size(); count++) {
			System.out.println(count + " Suit: " + deck.get(count).getSuit() + " Face: " + deck.get(count).getFace());
		}
	}
}
