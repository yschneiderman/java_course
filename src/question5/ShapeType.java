package question5;

public enum ShapeType {
	LINE,
	CIRCLE,
	RECTANGLE,
	ROUNDED_RECTANGLE
}
