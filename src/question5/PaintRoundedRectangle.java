package question5;

import java.awt.Color;
import java.awt.Graphics;

public class PaintRoundedRectangle extends PaintShape{

	public PaintRoundedRectangle(int x, int y, int width, int height, Color color, boolean isFilled) {
		super(x, y, width, height, color, isFilled);
		// TODO Auto-generated constructor stub
	}

	@Override
	void draw(Graphics graphics, int x, int y, int width, int height, Color color,  boolean isFilled) {
		graphics.setColor(color);
		if(isFilled) {
			graphics.fillRoundRect(x, y, width, height, 15, 15);
		}else {
			graphics.drawRoundRect(x, y, width, height, 15, 15);
		}	
	}

}
