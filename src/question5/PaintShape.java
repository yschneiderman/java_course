package question5;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public abstract class PaintShape extends JPanel{
	private Color color;
	private boolean isFilled ;
	private int width;
	private int height;
	private int x;
	private int y;
	
	
	public PaintShape(int x, int y, int width, int height , Color color,  boolean isFilled) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
		this.isFilled = isFilled;
	}
	
	abstract void draw(Graphics graphics, int x2, int y2, int width2, int height2, Color color2, boolean isFilled);


	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}



	public boolean isFilled() {
		return isFilled;
	}

	public void setFilled(boolean isFilled) {
		this.isFilled = isFilled;
	}

	public int getWidth() {
		return width;
	}


	public void setWidth(int width) {
		this.width = width;
	}


	public int getHeight() {
		return height;
	}


	public void setHeight(int height) {
		this.height = height;
	}


	public int getX() {
		return x;
	}


	public void setX(int x) {
		this.x = x;
	}


	public int getY() {
		return y;
	}


	public void setY(int y) {
		this.y = y;
	}

	
	
}


