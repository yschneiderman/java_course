package question5;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class NavbarPanelButtons {
	static ShapeType currentType;
	JButton circleBtn;
	JButton lineBtn;
	JButton recBtn;
	JButton roundeRecBtn;
	JButton colorBtn;
	JButton undoBtn;
	JButton clearBtn;
	JButton fillBtn;
	JButton exitBtn;
	
	static boolean isFilled = true;
	static String fillUnFillText = "Fill";
	
	void draw(Graphics graphics, int x, int y, int width, int height, Color color) {
		graphics.setColor(color);
		graphics.fillOval(x,y,height, width);
	}


	NavbarPanelButtons(JPanel panel) {
		
		this.circleBtn = new JButton("Circle");
		this.lineBtn = new JButton("Line");
		this.recBtn = new JButton("Rectangle");
		this.roundeRecBtn = new JButton("Rounde Rectangle");
		this.colorBtn = new JButton("Color");
		this.undoBtn = new JButton("Undo");
		this.clearBtn = new JButton("Clear");
		this.fillBtn = new JButton(fillUnFillText);
		this.exitBtn = new JButton("Exit");
		
		
		this.circleBtn.setPreferredSize(new Dimension(80, 20));
		this.lineBtn.setPreferredSize(new Dimension(80, 20));
		this.recBtn.setPreferredSize(new Dimension(80, 20));
		this.roundeRecBtn.setPreferredSize(new Dimension(80, 20));
		this.colorBtn.setPreferredSize(new Dimension(80, 20));
		this.undoBtn.setPreferredSize(new Dimension(80, 20));
		this.clearBtn.setPreferredSize(new Dimension(80, 20));
		this.fillBtn.setPreferredSize(new Dimension(80, 20));
		this.exitBtn.setPreferredSize(new Dimension(80, 20));

//		try {
//			this.circleBtn.setBorderPainted(false);
//			this.circleBtn.setOpaque(false);
//			this.circleBtn.setContentAreaFilled(false);
//			Image img = ImageIO.read(getClass().getResource("/question5/25477.png"));
//			Image newImg = img.getScaledInstance(35, 35, 0);
//		
//			this.circleBtn.setIcon(new ImageIcon(newImg));
//		}catch (Exception e) {
//			// TODO: handle exception
//		}
		
	
		
		panel.add(this.circleBtn);
		panel.add(this.lineBtn);
		panel.add(this.recBtn);
		panel.add(this.roundeRecBtn);
		panel.add(this.colorBtn);
		panel.add(this.undoBtn);
		panel.add(this.clearBtn);
		panel.add(this.fillBtn);
		panel.add(this.exitBtn);
		
		currentType = ShapeType.CIRCLE;
		
		
	}
	
	/******************************
	 * Adding shape button listener
	 *******************************/
	public void addShapeListner(JButton btn, ShapeType shape) {
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentType = shape;
			}
		});
	}
	
	/*****************************
	 * Adding exit button listener
	 *****************************/
	public void addExistBtnListner(JButton btn) {
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}
	
	public void addColorChooserListener() {
		this.colorBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ColorChooserDialog colorChooserDialog = new ColorChooserDialog();
				colorChooserDialog.openDialog();
			}
		});
	}
	
	
}
