package question5;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;

public class ColorChooserDialog {

  public void openDialog() {

    final JColorChooser colorChooser = new JColorChooser();
    final JLabel previewLabel = new JLabel("Test color", JLabel.CENTER);
    previewLabel.setFont(new Font("Serif", Font.BOLD | Font.ITALIC, 48));
    previewLabel.setSize(previewLabel.getPreferredSize());
    previewLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 1, 0));
    colorChooser.setPreviewPanel(previewLabel);

    ActionListener okActionListener = new ActionListener() {
      public void actionPerformed(ActionEvent actionEvent) {
        System.out.println("OK Button");
        Draw.setColor(colorChooser.getColor());
      }
    };

    ActionListener cancelActionListener = new ActionListener() {
      public void actionPerformed(ActionEvent actionEvent) {
        System.out.println("Cancel Button");
      }
    };

    final JDialog dialog = JColorChooser.createDialog(null, "Change Button Background", true,
        colorChooser, okActionListener, cancelActionListener);

    dialog.setVisible(true);
  }
}
 