package question5;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Draw extends JFrame {
	private ArrayList<PaintShape> shapes;
	private PaintShape tempShape = null;
	private static Color color = Color.BLACK;

	private NavbarPanelButtons navbarPanelButtons;
	private DrawPanel drawPanel;
	private JFrame frame;
	private JPanel panel;
	final int BUTTON_PANEL_HEIGHT = 40;
	

	int x1;
	int y1;
	int x2;
	int y2;

	public static void setColor(Color _color) {
		color = _color;
		System.out.println(color);
	}

	public Draw(String frameTitle) {

		this.shapes = new ArrayList<>();

		this.frame = new JFrame(frameTitle);
		this.drawPanel = new DrawPanel();
		this.panel = new JPanel();
		this.navbarPanelButtons = new NavbarPanelButtons(this.panel);

		this.reDraw();

	}

	public void reDraw() {

		this.frame.setSize(1200, 500);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.panel.setSize(1200, 40);
		
		this.addListeners();

		this.frame.add(this.panel);
		this.frame.add(this.drawPanel.panel);
		this.frame.setVisible(true);
	}



	public void addMouseMotionListener() {
		this.drawPanel.panel.addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				x2 = e.getX();
				y2 = e.getY();
				if(y1 >= BUTTON_PANEL_HEIGHT && y2 > BUTTON_PANEL_HEIGHT) {
					int widht = Math.abs(x1 - x2);
					int height = Math.abs(y1 - y2);

					tempShape = Draw.generateShape(x1, y1, x2, y2, height, widht);
					drawAllShapes();
				}			
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {
			}
		});
	}

	public void addUndoListener() {
		this.navbarPanelButtons.undoBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (shapes.size() > 0) {
					shapes.remove(shapes.size() - 1);
					drawAllShapes();
				}
			}
		});
	}
	
	public void addFillListener() {
		this.navbarPanelButtons.fillBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(NavbarPanelButtons.fillUnFillText.equals("Fill")) {
					NavbarPanelButtons.fillUnFillText = "UnFill";
				}else {
					NavbarPanelButtons.fillUnFillText = "Fill";
				}
				
				ChangeFillText();
			}
		});
	}
	
	
	
	public void addClearLitener() {
		this.navbarPanelButtons.clearBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				shapes.clear();
				clearPanel();
			}
		});

	}
	
	public void addMouseListener() {
		this.drawPanel.panel.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				x1 = e.getX();
				y1 = e.getY();
			}

			public void mouseReleased(MouseEvent e) {
				tempShape = null;
				x2 = e.getX();
				y2 = e.getY();
				if(y1 >= BUTTON_PANEL_HEIGHT) {
					y2 = y2 >= BUTTON_PANEL_HEIGHT ? y2 : BUTTON_PANEL_HEIGHT;
					int widht = Math.abs(x1 - x2);
					int height = Math.abs(y1 - y2);
					shapes.add(Draw.generateShape(x1, y1, x2, y2, height, widht));
					drawAllShapes();
				}
			}
		});
	}

	public void addListeners() {
		this.navbarPanelButtons.addExistBtnListner(this.navbarPanelButtons.exitBtn);
		this.navbarPanelButtons.addShapeListner(this.navbarPanelButtons.lineBtn, ShapeType.LINE);
		this.navbarPanelButtons.addShapeListner(this.navbarPanelButtons.recBtn, ShapeType.RECTANGLE);
		this.navbarPanelButtons.addShapeListner(this.navbarPanelButtons.roundeRecBtn, ShapeType.ROUNDED_RECTANGLE);
		this.navbarPanelButtons.addShapeListner(this.navbarPanelButtons.circleBtn, ShapeType.CIRCLE);
		this.navbarPanelButtons.addColorChooserListener();
		this.addFillListener();
		addMouseMotionListener();
		addUndoListener();
		addClearLitener();
		addMouseListener();
	}

	public static PaintShape generateShape(int x1, int y1, int x2, int y2, int widht, int height) {

		if (!NavbarPanelButtons.currentType.equals(ShapeType.LINE)) {
			x1 = x1 > x2 ? x2 : x1;
			y1 = y1 > y2 ? y2 : y1;
			x2 = x1 > x2 ? x1 : x2;
			y2 = y1 > y2 ? y1 : y2;
		}

		switch (NavbarPanelButtons.currentType) {
		case CIRCLE:
			return new PaintCircle(x1, y1, widht, height, Draw.color , NavbarPanelButtons.isFilled);
		case LINE:
			return new PaintLine(x1, y1, x2, y2, Draw.color, NavbarPanelButtons.isFilled);
		case RECTANGLE:
			return new PaintRectangle(x1, y1, height, widht, Draw.color,NavbarPanelButtons.isFilled);
		case ROUNDED_RECTANGLE:
			return new PaintRoundedRectangle(x1, y1, height, widht, Draw.color,NavbarPanelButtons.isFilled);
		default:
			return null;
		}
	}

	/*******************
	 * Clear draw panel
	 ********************/
	private void clearPanel() {
		this.drawPanel.panel.getGraphics().clearRect(0, BUTTON_PANEL_HEIGHT, this.drawPanel.panel.getWidth(), this.drawPanel.panel.getHeight());
	}

	/**************************************
	 * Draw Static shapes and temp shape
	 **************************************/
	private void drawAllShapes() {
		this.clearPanel();
		shapes.stream().forEach(shape -> {
			shape.draw(this.drawPanel.panel.getGraphics(), shape.getX(), shape.getY(), shape.getWidth(), shape.getHeight(),
					shape.getColor(),  shape.isFilled());
		});

		if (tempShape != null) {
			tempShape.draw(this.drawPanel.panel.getGraphics(), tempShape.getX(), tempShape.getY(), tempShape.getWidth(),
					tempShape.getHeight(), tempShape.getColor(), tempShape.isFilled());
		}
	}
	
	
	private void ChangeFillText() {
		NavbarPanelButtons.isFilled = !NavbarPanelButtons.isFilled;
		this.navbarPanelButtons.fillBtn.setText(NavbarPanelButtons.fillUnFillText);
	}	
}
