package question5;

import java.awt.Color;
import java.awt.Graphics;

public class PaintLine extends PaintShape {

	public PaintLine(int x, int y, int width, int height, Color color, boolean isFilled) {
		super(x, y, width, height, color, isFilled);
	}

	@Override
	void draw(Graphics graphics, int x1, int y1, int x2, int y2, Color color,  boolean isFilled) {
		graphics.setColor(color);
		graphics.drawLine(x1, y1,  x2,  y2);	
	}

}
