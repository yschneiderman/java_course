package question5;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class PaintCircle extends PaintShape{
	
	
	public PaintCircle(int x, int y, int width, int height, Color color, boolean isFilled) {
		super(x, y, width, height, color, isFilled);
	}

	@Override
	void draw(Graphics graphics, int x, int y, int width, int height, Color color, boolean isFilled) {
		graphics.setColor(color);
		if(isFilled) {
			graphics.fillOval(x,y,height, width);
		}else {
			graphics.drawOval(x,y,height, width);
		}	
	}	
}
                                                                                        