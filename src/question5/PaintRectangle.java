package question5;

import java.awt.Color;
import java.awt.Graphics;

public class PaintRectangle extends PaintShape{

	public PaintRectangle(int x, int y, int width, int height, Color color, boolean isFilled) {
		super(x, y, width, height, color, isFilled);
	}


	@Override
	void draw(Graphics graphics, int x, int y, int width, int height, Color color,  boolean isFilled) {
		graphics.setColor(color);
		if(isFilled) {
			graphics.fillRect(x, y, width,  height);
		}else {
			graphics.drawRect(x, y, width,  height);
		}	
	}
}
